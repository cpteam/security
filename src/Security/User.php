<?php

namespace CPTeam\Nette\Security;

use CPTeam\Model\IUser;
use CPTeam\Model\IUserRepository;
use Nette\Security as NS;

/**
 * @package CPTeam\Security
 */
class User extends NS\User
{
	
	/** @var IUserRepository */
	private $userRepository;
	
	/** @var IUser */
	private $entity = null;
	
	/**
	 * User constructor.
	 *
	 * @param NS\IUserStorage $storage
	 * @param NS\IAuthenticator|null $authenticator
	 * @param NS\IAuthorizator|null $authorizator
	 * @param IUserRepository $userRepository
	 */
	public function __construct(
		NS\IUserStorage $storage,
		NS\IAuthenticator $authenticator = null,
		NS\IAuthorizator $authorizator = null,
		IUserRepository $userRepository
	) {
		parent::__construct($storage, $authenticator, $authorizator);
		
		$this->userRepository = $userRepository;
	}
	
	/**
	 * @param bool $refresh
	 *
	 * @return User|\Nextras\Orm\Entity\IEntity|null
	 */
	public function getEntity($refresh = false)
	{
		if ($this->isLoggedIn() == false) {
			return null;
		}
		
		if ($refresh || $this->entity === null) {
			$this->entity = $this->userRepository->getById($this->getId());
		}
		
		return $this->entity;
	}
	
}
