<?php

namespace CPTeam\Nette\Security;

use CPTeam\Model\IUser;
use Nette\Security as NS;

/**
 * Class User
 *
 * @package CPTeam\Nette\Security
 */
class NextrasIdentity extends NS\Identity
{
	
	/**
	 * NextrasIdentity constructor.
	 *
	 * @param IUser $entity
	 * @param null $data
	 */
	public function __construct(IUser $entity, $data)
	{
		parent::__construct($entity->id, $entity->roleToArray(), $data);
	}
	
}

